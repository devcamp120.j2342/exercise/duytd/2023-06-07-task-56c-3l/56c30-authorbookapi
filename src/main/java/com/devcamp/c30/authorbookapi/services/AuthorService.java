package com.devcamp.c30.authorbookapi.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import com.devcamp.c30.authorbookapi.models.Author;

@Service
public class AuthorService {

    Author author1 = new Author("Dang Vuong Hung", "hung@mail.com", 'm');
    Author author2 = new Author("Duong Cong Minh", "minh@mail.com", 'f');
    Author author3 = new Author("Nguyen Phong", "phong@mail.com", 'm');
    Author author4 = new Author("Tran Thanh Hai", "hai@mail.com", 'f');
    Author author5 = new Author("Thich Nhat Hanh", "hanh@mail.com", 'm');
    Author author6 = new Author("Nguyen Du", "dunguyen@mail.com", 'f');

    Author author7 = new Author("Dang Vuong Hung", "hung@mail.com", 'm');
    Author author8 = new Author("Duong Cong Minh", "minh@mail.com", 'f');
    Author author9 = new Author("Nguyen Phong", "phong@mail.com", 'm');
    Author author10 = new Author("Tran Thanh Hai", "hai@mail.com", 'f');
    Author author11 = new Author("Thich Nhat Hanh", "hanh@mail.com", 'm');
    Author author12 = new Author("Nguyen Du", "dunguyen@mail.com", 'f');

    public ArrayList<Author> getAuthorBook1() {
        ArrayList<Author> authorBook1 = new ArrayList<>();

        authorBook1.add(author1);
        authorBook1.add(author2);

        return authorBook1;
    }

    public ArrayList<Author> getAuthorBook2() {
        ArrayList<Author> authorBook2 = new ArrayList<>();

        authorBook2.add(author3);
        authorBook2.add(author4);

        return authorBook2;
    }

    public ArrayList<Author> getAuthorBook3() {
        ArrayList<Author> authorBook3 = new ArrayList<>();

        authorBook3.add(author5);
        authorBook3.add(author6);

        return authorBook3;
    }

    public ArrayList<Author> getAuthorBook4() {
        ArrayList<Author> authorBook1 = new ArrayList<>();

        authorBook1.add(author7);
        authorBook1.add(author8);

        return authorBook1;
    }

    public ArrayList<Author> getAuthorBook5() {
        ArrayList<Author> authorBook2 = new ArrayList<>();

        authorBook2.add(author9);
        authorBook2.add(author10);

        return authorBook2;
    }

    public ArrayList<Author> getAuthorBook6() {
        ArrayList<Author> authorBook3 = new ArrayList<>();

        authorBook3.add(author11);
        authorBook3.add(author12);

        return authorBook3;
    }

    public ArrayList<Author> getAllAuthors() {
        ArrayList<Author> allAuthor = new ArrayList<>();

        allAuthor.add(author1);
        allAuthor.add(author2);
        allAuthor.add(author3);
        allAuthor.add(author4);
        allAuthor.add(author5);
        allAuthor.add(author6);

        allAuthor.add(author7);
        allAuthor.add(author8);
        allAuthor.add(author9);
        allAuthor.add(author10);
        allAuthor.add(author11);
        allAuthor.add(author12);

        return allAuthor;
    }

    public ArrayList<Author> getAuthorInfo(@RequestParam(name = "code", required = true) String email) {
        ArrayList<Author> allAuthors = getAllAuthors();
        ArrayList<Author> foundAuthors = new ArrayList<Author>();
        for (Author authorElement : allAuthors) {
            if (authorElement.getEmail().equals(email)) {
                foundAuthors.add(authorElement);
            }

        }
        return foundAuthors;
    }

    public ArrayList<Author> getAuthorsByGender(@RequestParam(name = "code", required = true) char gender) {
        ArrayList<Author> allAuthors = getAllAuthors();
        ArrayList<Author> foundAuthorsByGender = new ArrayList<>();

        for (Author author : allAuthors) {
            if (author.getGender() == gender) {
                foundAuthorsByGender.add(author);
            }
        }

        return foundAuthorsByGender;
    }
}
