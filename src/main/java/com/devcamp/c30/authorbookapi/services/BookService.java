package com.devcamp.c30.authorbookapi.services;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import com.devcamp.c30.authorbookapi.models.Book;

@Service
public class BookService {
    @Autowired
    AuthorService authorService;

    public ArrayList<Book> getAllBooks(){
        ArrayList<Book> allBooks = new ArrayList<>();

        Book book1 = new Book("Book1", authorService.getAuthorBook1(), 100000, 1);
        Book book2 = new Book("Book2", authorService.getAuthorBook2(), 200000, 2);
        Book book3 = new Book("Book3", authorService.getAuthorBook3(), 300000, 3);

        Book book4 = new Book("Book4", authorService.getAuthorBook4(), 100000, 4);
        Book book5 = new Book("Book5", authorService.getAuthorBook5(), 200000, 5);
        Book book6 = new Book("Book6", authorService.getAuthorBook6(), 300000, 6);

        allBooks.add(book1);
        allBooks.add(book2);
        allBooks.add(book3);

        allBooks.add(book4);
        allBooks.add(book5);
        allBooks.add(book6);

        return allBooks;
    }

    public ArrayList<Book> getBookQuantity(@RequestParam(name="code", required = true) int qty){
        ArrayList<Book> allBooks = getAllBooks();
        ArrayList<Book> foundBooks = new ArrayList<>();

        for (Book bookElement : allBooks) {
            if (bookElement.getQty() == qty) {
                foundBooks.add(bookElement);
            }
        }

        return foundBooks;
    }

    public Book getBookByindex(int index){
        ArrayList<Book> allBook = getAllBooks();
        if(index >= 0 && index < allBook.size()){
            return allBook.get(index);
        }
        return null;
    }

}
