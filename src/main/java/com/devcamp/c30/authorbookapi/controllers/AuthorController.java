package com.devcamp.c30.authorbookapi.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.c30.authorbookapi.models.Author;
import com.devcamp.c30.authorbookapi.services.AuthorService;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class AuthorController {
    @Autowired
    AuthorService authorService;

    @GetMapping("/author-info")
    public ArrayList<Author> getAuthorInfo(@RequestParam(name="code", required = true)String emial){
        ArrayList<Author> authorInfo = authorService.getAuthorInfo(emial);

        return authorInfo;
    }

    @GetMapping("/author-gender")
    public ArrayList<Author> getAuthorByGender(@RequestParam(name="code", required = true) char gender){
        ArrayList<Author> authorByGender = authorService.getAuthorsByGender(gender);

        return authorByGender;
    }
}
