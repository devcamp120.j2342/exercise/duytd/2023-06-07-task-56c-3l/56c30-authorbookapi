package com.devcamp.c30.authorbookapi.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.c30.authorbookapi.models.Book;
import com.devcamp.c30.authorbookapi.services.BookService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class BookController {
    @Autowired
    BookService bookService;

    @GetMapping("/books")
    public ArrayList<Book> getAllBooks(){
        ArrayList<Book> allBooks = bookService.getAllBooks();

        return allBooks;
    }

    @GetMapping("/book-quantyti")
    public ArrayList<Book> getBookByQuantyti(@RequestParam(name="code", required=true) int qty) {
        ArrayList<Book> bookQuantyti = bookService.getBookQuantity(qty);

        return bookQuantyti;
    }

    @GetMapping("/books/{bookId}")
    public Book getBookByIndex(@PathVariable("bookId") int index){
        return bookService.getBookByindex(index);
    }
}
